# Project "Phonebook. One hundred million entries. (Server)"

### Запуск проекта:
1. Установка необходимых инструментов:
    - установка ProtoBuf. В терминале:
    <pre>
    go get github.com/golang/protobuf/protoc-gen-go
    </pre>
    Подробо о том, как создать проект c использованием ProtoBuf можно ознакомится по [*ссылке*] (https://gitlab.com/jaroslav.kuchmij/phoneBookProtoBuf)
    - установка BoltDB. В терминале
    <pre>
    go get github.com/boltdb/bolt/...
    </pre>
    - установк gRPC. В терминале
    <pre>
    go get google.golang.org/grpc
    </pre>
    
2. Скачать проект 
<pre>
go get gitlab.com/jaroslav.kuchmij/phoneBookOneHundredMillionEntries_Server.git
</pre>

#### Запуск StreamOneContactsFromManyFiles
1. Перейти в пакет: 
%GOPATH%/src/gitlab.com/jaroslav.kuchmij/phoneBookOneHundredMillionEntries_Server.git/StreamOneContactsFromManyFiles
и запустить команду:
<pre>
go run main.go
</pre>

2. Перейти в пакет:
%GOPATH%/src/gitlab.com/jaroslav.kuchmij/phoneBookOneHundredMillionEntries_Server.git/StreamOneContactsFromManyFiles/settings
и указать настройки для для для записи в BoltDB


**~~Создатель кода~~** Главный бездельник - [*YaroslavKuchmiy*](https://www.facebook.com/profile.php?id=100006520172879)