package main

import (
	"log"
	"net"
	"google.golang.org/grpc"
	"github.com/spf13/viper"
	"io"
	"gitlab.com/jaroslav.kuchmij/phoneBookOneHundredMillionEntries_Server.git/StreamOneContactsFromManyFiles/rrs"
	"github.com/boltdb/bolt"
	"github.com/golang/protobuf/proto"
)

type server struct {}

var  (
	port string // порт подключения по TCP
	nameDB string //название БД
	nameBucket string //название корзины в БД
	sizeBatch int //размер пачки для последующего коммита (должен равнят)
	maxStep int // количество транзакций
	goodResponse = &rrs.MessResponse{Message: "200 Ok"}
	count = 1
)

func main()  {
	//получение данных из конфиг файла
	err := getConfig()
	if err != nil {
		log.Fatalf("failed to getConfig: %v", err)
	}

	//старт сервера
	err =runGRPC()
	if err != nil {
		log.Fatalf("failed to runGRPC: %v", err)
	}
}

func runGRPC() error  {
	log.Println("Start server...")
	//Открытие порта
	lis, err := net.Listen("tcp", port)
	if err != nil {
		return err
	}
	// Creates a new gRPC server
	s := grpc.NewServer()
	rrs.RegisterPhoneBookServer(s, &server{})
	if err := s.Serve(lis); err != nil {
		return err
	}
	return nil
}

func (s *server) SetAllContactsInBolt(stream rrs.PhoneBook_SetAllContactsInBoltServer)  error {
	log.Println("Set contacts in Bolt")
	//счетчик шагов для коммита
	var step = 0
	//Открытие БД
	db, err := bolt.Open(nameDB, 0600, nil) //открытие БД
	if err != nil{
		log.Printf("err open DB: %v", err)
		panic(err)
	}
	defer db.Close()//После выполнения команды - закрытие БД

	var maxBatch = sizeBatch // максимальный придел пачки, после чего происходит коммит
	for step < maxStep {
		err = db.Update(func(tx *bolt.Tx) error {
			_, err := tx.CreateBucketIfNotExists([]byte(nameBucket)) //создание Bucket
			if err != nil {
				return err
			}
			for {
				//получение данных от клиента
				contact, err := stream.Recv()
				//если счетчик равен максимальной патчке, тогда коммит
				if count == maxBatch  {
					maxBatch = maxBatch + sizeBatch
					log.Printf("count: %v", count)
					return nil
				}
				//если нет следующего элементе от клиента, тогда возврат ответа клиенту и закрытие канала
				if err == io.EOF {
					return stream.SendAndClose(goodResponse)
				}
				if err != nil {
					log.Println("\t error in stream.Recv(): ", err)
					return err
				}
				//маршалинг данных
				mrshlOut, err := proto.Marshal(contact.Contact) //перевод Структуры в формат ProtoBuf (сирилизация)
				if err != nil {
					return err
				}
				//добавление в корзину данных
				err = tx.Bucket([]byte(nameBucket)).Put([]byte(contact.Contact.Number), mrshlOut) //Получение необходимой корзины и добавление новой записи
				if err != nil {
					return err
				}
				count++
			}
			return nil
		})
		if err != nil {
			log.Printf("update: %v", err)
			panic(err)
		}
		step++
	}
	return nil
}

// Получение данных из сонфигурационного файла
func getConfig() error {
	//название конфиг файла
	viper.SetConfigName("config")
	//путь к конф файлу
	viper.AddConfigPath("settings")

	//читаем конф файл
	if err := viper.ReadInConfig(); err != nil {
		return err
	//получение настроек из конф файла и запись в локальные переменные
	} else {
		port = viper.GetString("server.port")
		nameDB = viper.GetString("server.nameDB")
		nameBucket = viper.GetString("server.nameBucket")
		sizeBatch = viper.GetInt("server.sizeBatch")
		maxStep = viper.GetInt("server.maxStep")
	}
	return nil
}